package org.nyna.messenger.resources.api;

import java.util.Collection;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.nyna.messenger.resources.model.Comment;
import org.nyna.messenger.resources.store.CommentStore;
import org.nyna.messenger.resources.store.MessageStore;

@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class CommentsAPI {

    /**
     * Method handling HTTP GET requests for retrieving all comments of the specified message.
     * @return A collection of comments that will be returned as a JSON response.
     */
    @GET
	public Collection<Comment> getAllComments(@PathParam("messageId") long messageId) {
		return CommentStore.retrieveAllComments(MessageStore.retrieveMessage(messageId));
	}
    
    /**
     * Method handling HTTP GET requests for retrieving a single comment of the specified message.
     * @return The message specified in the URI that will be returned as a JSON response.
     */
    @GET
    @Path("/{commentId}")
    public Comment getMessage(@PathParam("messageId") long messageId, @PathParam("commentId") long commentId) {
    	return CommentStore.retrieveComment(MessageStore.retrieveMessage(messageId), commentId);
    }
}
