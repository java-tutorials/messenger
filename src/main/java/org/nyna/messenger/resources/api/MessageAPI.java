package org.nyna.messenger.resources.api;

import java.net.URI;
import java.util.Collection;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriInfo;

import org.nyna.messenger.resources.model.Message;
import org.nyna.messenger.resources.store.MessageStore;

@Path("/messages")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class MessageAPI {

    /**
     * Method handling HTTP GET requests for retrieving all messages of the Messenger store.
     * @return A collection of messages that will be returned as a JSON response.
     */
    @GET
	public Collection<Message> getAllMessages() {
		return MessageStore.retrieveAllMessages();
	}
    
    /**
     * Method handling HTTP GET requests for retrieving a single message of the Messenger store.
     * @return The HTTP response with the body containing the message specified in the URI.
     */
    @GET
    @Path("/{messageId}")
    public Response getMessage(@PathParam("messageId") long messageId) {
    	Message message = MessageStore.retrieveMessage(messageId);
    	return Response.status(Status.OK)
    				   .entity(message)
    				   .build();
    }
    
    /**
     * Method handling HTTP POST requests for storing a new message in the Messenger store.
     * @return The HTTP response with the body containing the stored message.
     */
    @POST
    public Response storeMessage(Message message, @Context UriInfo uriInfo) {
    	long messagedId = MessageStore.storeMessage(message);
    	
    	//Prepare the URI location for accessing the newly stored message
    	URI location = uriInfo.getAbsolutePathBuilder()
    						  .path(String.valueOf(messagedId))
    						  .build();
    	
    	//Prepare and return the HTTP response with the URI location
    	return Response.created(location)
    			.entity(message)
    			.build();
    }
    
    /**
     * Method handling HTTP PUT requests for updating a single message of the Messenger store.
     * @return The original message prior update that will be returned as a JSON response.
     */
    @PUT
    @Path("/{messageId}")
    public Message updateMessage(@PathParam("messageId") long messageId, Message message) {
    	message.setId(messageId);
    	MessageStore.storeMessage(message);
    	return message;
    }
    
    /**
     * Method handling HTTP DELETE requests for removing a single message of the Messenger store.
     * @return The deleted message that will be returned as a JSON response.
     */
    @DELETE
    @Path("/{messageId}")
    public Message removeMessage(@PathParam("messageId") long messageId) {
    	return MessageStore.deleteMessage(messageId);
    }
    
    /**
     * Method passing all REST API calls to the sub-resource comments located under messages
     * @return an instance of comments sub-resource
     */
    @Path("/{messageId}/comments")
    public CommentsAPI commentsSubResource() {
    	return new CommentsAPI();
    }
}
