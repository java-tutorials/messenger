package org.nyna.messenger.resources.api;

import java.util.Collection;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.nyna.messenger.resources.model.Profile;
import org.nyna.messenger.resources.store.ProfileStore;

@Path("/profiles")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class ProfileAPI {

    /**
     * Method handling HTTP GET requests for retrieving all profiles from the Profile store.
     * @return A collection of profiles that will be returned as a JSON response.
     */
    @GET
	public Collection<Profile> getAllProfiles() {
		return ProfileStore.retrieveAllProfiles();
	}
    
    /**
     * Method handling HTTP GET requests for retrieving a single profile from the Profile store.
     * @return The profile specified in the URI that will be returned as a JSON response.
     */
    @GET
    @Path("/{profileId}")
    public Profile getProfile(@PathParam("profileId") String profileId) {
    	return ProfileStore.retrieveProfile(profileId);
    }
    
    /**
     * Method handling HTTP POST requests for storing a new profile in the Profile store.
     * @return The new posted profile that will be returned as a JSON response.
     */
    @POST
    public Profile storeProfile(Profile profile) {
    	ProfileStore.storeProfile(profile);
    	return profile;
    }
    
    /**
     * Method handling HTTP PUT requests for updating a single profile of the Profile store.
     * @return The original profile prior update that will be returned as a JSON response.
     */
    @PUT
    @Path("/{profileId}")
    public Profile updateProfile(@PathParam("profileId") String profileId, Profile profile) {
    	profile.setId(profileId);
    	ProfileStore.storeProfile(profile);
    	return profile;
    }
    
    /**
     * Method handling HTTP DELETE requests for removing a single profile of the Profile store.
     * @return The deleted profile that will be returned as a JSON response.
     */
    @DELETE
    @Path("/{profileId}")
    public Profile removeProfile(@PathParam("profileId") String profileId) {
    	return ProfileStore.deleteProfile(profileId);
    }
}
