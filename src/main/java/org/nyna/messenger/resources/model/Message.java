package org.nyna.messenger.resources.model;

import java.util.Date;

import org.nyna.messenger.resources.store.MessageStore;

//import javax.xml.bind.annotation.XmlRootElement;

//@XmlRootElement
/**
 * A class for creating simple messages on our RESTfull WebService.
 * @author Jonathan Puvilland
 *
 */
public class Message {

	private long id;
	private String message;
	private String author;
	private Date createdOn;

	/**
	 * Empty constructor. Required by the Jersey API.
	 */
	public Message() {

	}
	
	/**
	 * Creates a simple message.
	 * @param message the text of the message.
	 * @param author the author of the message.
	 */
	public Message (String message, String author) {
		this.id = MessageStore.getStoreSize()+1;
		this.message = message;
		this.author = author;
		this.createdOn = new Date();
	}

	/**
	 * @return the internal id of the message.
	 */
	public long getId() {
		return id;
	}

	/**
	 * Sets the internal id of the message.
	 * @param id the internal id of the message.
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * @return the message's text.
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * Sets the text of the message.
	 * @param message the text to set in the message.
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * @return the author of the message.
	 */
	public String getAuthor() {
		return author;
	}

	/**
	 * Sets the author of the message.
	 * @param author the author of the message.
	 */
	public void setAuthor(String author) {
		this.author = author;
	}

	/**
	 * @return the creation date of the message.
	 */
	public Date getCreatedOn() {
		return createdOn;
	}

	/**
	 * Sets the creation date of the message.
	 * @param createdOn the creation date to set.
	 */
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	
	@Override
	public boolean equals(Object message) {
		if(message.getClass() != Message.class)
			return false;
		
		Message other = (Message) message;
		
		return  this.id == other.id &&
				this.author.equals(other.author) &&
				this.message.equals(other.message);
	}

}