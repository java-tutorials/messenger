package org.nyna.messenger.resources.model;

import java.util.Date;

//import javax.xml.bind.annotation.XmlRootElement;

//@XmlRootElement
/**
 * A Messenger application comment on a message.
 * @author Jonathan Puvilland
 *
 */
public class Comment {
	private long id;
	private String author;
	private String comment;
	private Date createdOn;
	
	/**
	 * Empty constructor. Required by the Jersey API.
	 */
	public Comment() {
		
	}
	
	/**
	 * Creates a new comment, with provided author and comment text.
	 * @param author the author of the comment
	 * @param comment the text of the comment
	 */
	public Comment(String author, String comment) {
		this.id = 0L;
		this.author = author;
		this.comment = comment;
		this.createdOn = new Date();
	}
	
	/**
	 * @return the author
	 */
	public String getAuthor() {
		return author;
	}

	/**
	 * @param author the author to set
	 */
	public void setAuthor(String author) {
		this.author = author;
	}

	/**
	 * @return the comment
	 */
	public String getComment() {
		return comment;
	}

	/**
	 * @param comment the comment to set
	 */
	public void setComment(String comment) {
		this.comment = comment;
	}

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}
	
	/**
	 * @param the id to set
	 */
	public void setId(Long id) {
		this.id =  id;
	}

	/**
	 * @return the createdOn
	 */
	public Date getCreatedOn() {
		return createdOn;
	}
	
	public void setCreatedOn(Date date) {
		this.createdOn = date;
		
	}
	
	public String toString() {
		return "[" + this.id + ", " + this.author + ", " + this.comment + "]";
	}

	public boolean equals(Object profile) {
		if(profile.getClass() != Comment.class)
			return false;
		
		Comment other = (Comment) profile;
		
		return  this.id == other.id &&
				this.author.equals(other.author) &&
				this.comment.equals(other.comment);
	}
}
