package org.nyna.messenger.resources.model;

import java.util.Date;

//import javax.xml.bind.annotation.XmlRootElement;

//@XmlRootElement
/**
 * A Messenger application user profile.
 * @author Jonathan Puvilland
 *
 */
public class Profile {
	private String id;
	private String lastName;
	private String firstName;
	private String quoteOfTheDay;
	private Date createdOn;
	
	/**
	 * Empty constructor. Required by the Jersey API.
	 */
	public Profile() {
		
	}
	
	public Profile(String firstName, String lastName) {
		this.id = firstName + "." + lastName;
		this.firstName = firstName;
		this.lastName = lastName;
		this.createdOn = new Date();
	}
	
	public Profile(String firstName, String lastName, String quoteOfTheDay) {
		this(firstName, lastName);
		this.setQuoteOfTheDay(quoteOfTheDay);
	}
	
	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}
	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}
	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	/**
	 * @return the createdOn
	 */
	public Date getCreatedOn() {
		return createdOn;
	}

	/**
	 * @param createdOn the createdOn to set
	 */
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	
	/**
	 * @return the quoteOfTheDay
	 */
	public String getQuoteOfTheDay() {
		return quoteOfTheDay;
	}

	/**
	 * @param quoteOfTheDay the quoteOfTheDay to set
	 */
	public void setQuoteOfTheDay(String quoteOfTheDay) {
		this.quoteOfTheDay = quoteOfTheDay;
	}
	
	public boolean equals(Object profile) {
		if(profile.getClass() != Profile.class)
			return false;
		
		Profile other = (Profile) profile;
		
		return  this.id == other.id &&
				this.firstName.equals(other.firstName) &&
				this.lastName.equals(other.lastName);
	}
}
