package org.nyna.messenger.resources.store;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.nyna.messenger.resources.model.Comment;
import org.nyna.messenger.resources.model.Message;

/**
 * Static class that simulates a data store for storing and retrieving Messenger's objects like Profiles, Messages, etc.
 * @author Jonathan Puvilland
 *
 */
public class CommentStore {

	//Store for one message a list of multiple comments
	private static Map<Message, List<Comment>> comments = new HashMap<Message, List<Comment>>();

	/**
	 * Initializes the Messenger data store with some objects
	 */
	public static void init() {
		//Check if MessageStore is initialized, as messages must exists before creating comments.
		if(MessageStore.getStoreSize() == 0)
			MessageStore.init();	
		
		Message m1 = MessageStore.retrieveMessage(1);
		Message m2 = MessageStore.retrieveMessage(2);
		
		storeComment(m1, new Comment("James", "Comment 1"));
		storeComment(m1, new Comment("Albert", "Comment 2"));
		storeComment(m2, new Comment("Pierre", "Comment 1"));
		storeComment(m2, new Comment("Paul", "Comment 2"));
		storeComment(m2, new Comment("Jacques", "Comment 3"));
	}

	/**
	 * Store a comment in the data store
	 * @param message the message for which the comment will be stored
	 * @param comment the comment to store
	 * @return the internal id of the comment
	 */
	public static long storeComment(Message message, Comment comment) {
		// Checks if comment is a newly created comment, without internal id
		if(comment.getId() == 0)
			comment.setId(getStoreSize(message)+1);
		comment.setCreatedOn(new Date());
		
		List<Comment> messageComments;
		
		// If the message has not comment yet, initialize a list of comments and add it to the message
		if(CommentStore.retrieveAllComments(message) == null) {
			messageComments = new ArrayList<Comment>();
			comments.put(message, messageComments);
		}
		else
			messageComments = CommentStore.retrieveAllComments(message);
		
		messageComments.add(comment);
		
		return comment.getId();
	}
	
	/**
	 * Removes a comment from the store
	 * @param message the message for which the comment has to be retrieved
	 * @Param commentId the id of the comment to retrieve
	 * @return the comment retrieved from the store
	 */
	public static Comment deleteComment(Message message, Comment comment) {
		comments.get(message).remove(comment);
		return comment;
	}

	/**
	 * Retrieves a comment for a message from the store
	 * @param message the message for which the comment has to be retrieved
	 * @Param commentId the id of the comment to retrieve
	 * @return the comment retrieved from the store
	 */
	public static Comment retrieveComment(Message message, long commentId) {
		try {
			return comments.get(message).get((int) (commentId-1));
		} catch(IndexOutOfBoundsException e) {
			return null;
		}
	}


	/**
	 * Retrieves all comments from the store for the given message
	 * @return 
	 * @Param message the message for which the comments have to be retrieved
	 * @return a collection of all the comments in the store
	 */
	public static List<Comment> retrieveAllComments(Message message) {
		return comments.get(message);
	}
	
	/**
	 * Calculates the number of comments in the store for a message
	 * @param message the message for which the number of comments is to be calculated.
	 * @return the number of comments in the store forthe provided message
	 */
	public static long getStoreSize(Message message) {
		if(comments.get(message) == null)
			return 0;
		else
			return comments.get(message).size();
	}

}