package org.nyna.messenger.resources.store;

import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.nyna.messenger.resources.model.Message;

/**
 * Static class that simulates a data store for storing and retrieving Messenger's objects like Profiles, Messages, etc.
 * @author Jonathan Puvilland
 *
 */
public class MessageStore {

	private static Map<Long, Message> messages = new HashMap<Long, Message>();

	/**
	 * Initializes the Messenger data store with some objects
	 */
	public static void init() {
		storeMessage(new Message("I Love Java", "Nyna"));
		storeMessage(new Message("I learn REST API with Jersey", "Nyna"));
	}

	/**
	 * Store a message in the data store
	 * @param message the message to store
	 * @return the id of the message being created
	 */
	public static long storeMessage(Message message) {
		// Checks if message is a newly created message, without internal id
		if(message.getId() == 0)
			message.setId(getStoreSize()+1);
		message.setCreatedOn(new Date());
		messages.put(message.getId(), message);
		return message.getId();
	}
	
	/**
	 * Removes a message from the store
	 * @param messageId The id of the message to remove
	 * @return the deleted message
	 */
	public static Message deleteMessage(long messageId) {
    	Message message = MessageStore.retrieveMessage(messageId);
    	messages.remove(messageId);
    	return message;
	}

	/**
	 * Retrieves a message from the store
	 * @param messageId The id of the message to retrieve
	 * @return the message retrieved from the store
	 */
	public static Message retrieveMessage(long messageId) {
		return messages.get(messageId);
	}

	/**
	 * Retrieves all messages from the store
	 * @return a collection of all the messages in the store
	 */
	public static Collection<Message> retrieveAllMessages() {
		return messages.values();
	}
	
	/**
	 * Calculates the number of messages in the store
	 * @return the number of messages in the store
	 */
	public static int getStoreSize() {
		return messages.size();
	}

}