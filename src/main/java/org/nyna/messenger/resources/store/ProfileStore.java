package org.nyna.messenger.resources.store;

import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.nyna.messenger.resources.model.Profile;

/**
 * Static class that simulates a data store for storing and retrieving Messenger's objects like Messages, Profiles, etc.
 * @author Jonathan Puvilland
 *
 */
public class ProfileStore {

	private static Map<String, Profile> profiles = new HashMap<String, Profile>();

	/**
	 * Initializes the Messenger data store with some objects
	 */
	public static void init() {
		storeProfile(new Profile("Nynaeve", "Al'Mera", "Java rocks with Jersey"));
		storeProfile(new Profile("James", "Blunt", "You're beautifull"));
	}

	/**
	 * Store a Profile in the data store
	 * @param profile the Profile to store
	 */
	public static String storeProfile(Profile profile) {
		// Checks if profile is already present in the collection
		if(profile == null || profiles.get(profile.getId()) != null)
			return null;
		
		profile.setCreatedOn(new Date());
		profiles.put(profile.getId(), profile);
		return profile.getId();
	}
	
	/**
	 * Removes a Profile from the store
	 * @param profileId The id of the Profile to remove
	 * @return the deleted Profile
	 */
	public static Profile deleteProfile(String profileId) {
    	Profile profile = ProfileStore.retrieveProfile(profileId);
    	profiles.remove(profileId);
    	return profile;
	}

	/**
	 * Retrieves a Profile from the store
	 * @param profileId The id of the Profile to retrieve
	 * @return the Profile retrieved from the store
	 */
	public static Profile retrieveProfile(String profileId) {
		return profiles.get(profileId);
	}

	/**
	 * Retrieves all Profiles from the store
	 * @return a collection of all the Profiles in the store
	 */
	public static Collection<Profile> retrieveAllProfiles() {
		return profiles.values();
	}
	
	/**
	 * Calculates the number of Profiles in the store
	 * @return the number of Profiles in the store
	 */
	public static int getStoreSize() {
		return profiles.size();
	}

}