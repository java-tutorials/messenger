package org.nyna.messenger;

import java.io.IOException;
import java.net.URI;

import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;
import org.glassfish.jersey.server.ResourceConfig;
import org.nyna.messenger.resources.store.CommentStore;
import org.nyna.messenger.resources.store.MessageStore;
import org.nyna.messenger.resources.store.ProfileStore;

public class Messenger {
	// Base URI the Grizzly HTTP server will listen on
    public static final String BASE_URI = "http://localhost:8080/messenger/webapi";

    /**
     * Starts Grizzly HTTP server exposing JAX-RS resources defined in this application.
     * @return Grizzly HTTP server.
     */
    public static HttpServer startServer() {
        // create a resource configuration that scans for JAX-RS resources and providers
        // in org.nyna.messenger package
        final ResourceConfig rc = new ResourceConfig().packages("org.nyna.messenger");

        // create and start a new instance of grizzly http server
        // exposing the Jersey application at BASE_URI
        return GrizzlyHttpServerFactory.createHttpServer(URI.create(BASE_URI), rc);
    }

    /**
     * Initialises the Messenger data store with default messages, profiles and comments.
     */
    private static void initStore() {
    	MessageStore.init();
    	ProfileStore.init();
    	CommentStore.init();
    }

    /**
     * Main method.
     * @param args
     * @throws IOException
     */
    public static void main(String[] args) throws IOException {
    	initStore();
        final HttpServer server = startServer();
        System.out.println(String.format("Jersey app started with WADL available at "
                + "%sapplication.wadl\nHit enter to stop it...", BASE_URI));
        System.in.read();
        server.shutdownNow();
    }

}
