package com.proximus.bdp.http;

import java.io.IOException;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Simple Web HTTP client using Java REST API extension JAX RS.
 * The WebHDFS Client calls Apache WebHDFS methods for manipulating files on HDFS.
 * Main Class: WebHDFSClient.main
 * Args:
 *    - URI: the URI of the WebHDFS service to invoque
 * @author Jonathan Puvilland
 *
 */
public class WebHDFSClient {
	private static Client httpClient;
	private static WebTarget webTarget;
	
	/**
	 * Initializes the httpClient and the webTarget the Web URI provided in the command line
	 * @param uri the URI of the WebHDFS service
	 */
	public WebHDFSClient(String uri) {
		httpClient = ClientBuilder.newClient();
		webTarget = httpClient.target(uri);
	}

	/**
	 * Performs a HTTP GET from the WebHDFS service
	 * @param path the path from the HDFS root
	 * @return the received HTTP Response from the WebHDFS service
	 * @throws IOException
	 */
	private Response httpGet(String path) throws IOException {
		Invocation.Builder builder = webTarget.path(path).request(MediaType.APPLICATION_JSON);
		return builder.get();
	}
	
	/**
	 * Performs a HTTP POST from the WebHDFS service
	 * @param path the path from the HDFS root
	 * @param entity the object to post on the HDFS root
	 * @return the received HTTP Response from the WebHDFS service
	 * @throws IOException
	 */
	private Response httpPost(String path, String entity) throws IOException {
		Invocation.Builder builder = webTarget.path(path).request(MediaType.APPLICATION_JSON);
		return builder.post(Entity.entity(entity, MediaType.APPLICATION_JSON));
	}
	
	public static void main(String[] args) throws IOException {
		if(args.length != 1) {
			System.out.println("Usage: java $MAIN_CLASS $HOST_URI");
			System.exit (-1);
		}
		
		WebHDFSClient webHdfsClient = new WebHDFSClient(args[0]);
		Response response;
		String responseBody;
		
		// Get Method
		System.out.println("Getting from URI 1...");
		response = webHdfsClient.httpGet("1");
		responseBody = response.readEntity(String.class);
		System.out.println("Return Code: " + response.getStatus());
		System.out.println("Headers:\n" + response.getHeaders());
		System.out.println("Body:" + responseBody);

		// Post Method
		System.out.println("Posting back...");
		response = webHdfsClient.httpPost("", responseBody);
		responseBody = response.readEntity(String.class);
		System.out.println("Return Code: " + response.getStatus());
		System.out.println("Headers:\n" + response.getHeaders());
		System.out.println("Body:" + responseBody);
		
		httpClient.close();
	}
}
