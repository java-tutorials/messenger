package org.nyna.messenger.resources.store;

import org.junit.Test;
import org.nyna.messenger.resources.model.Message;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class MessageStoreTest {
    @Test
    public void messageShouldBeStored() {
    	Message myMessage = new Message("This is a new message", "Nyna");
    	long messageId = MessageStore.storeMessage(myMessage);
    	assertEquals(myMessage, MessageStore.retrieveMessage(messageId));
    }
    
    @Test
    public void messageShouldBeDeleted() {
    	Message myMessage = new Message("This is a message to delete", "Nyna");
    	long messageId = MessageStore.storeMessage(myMessage);
    	MessageStore.deleteMessage(messageId);
    	assertNull(MessageStore.retrieveMessage(myMessage.getId()));
    }
}