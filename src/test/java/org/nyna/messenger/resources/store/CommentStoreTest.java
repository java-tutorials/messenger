package org.nyna.messenger.resources.store;

import static org.junit.Assert.*;

import org.junit.BeforeClass;
import org.junit.Test;
import org.nyna.messenger.resources.model.Comment;
import org.nyna.messenger.resources.model.Message;

public class CommentStoreTest {
	
	@BeforeClass
	public static void initStore() {
		MessageStore.init();
		CommentStore.init();
	}
	
	@Test
	public void commentShouldBeStored() {
		Message m1 = MessageStore.retrieveMessage(1);
		Comment c1 = new Comment("Nyna", "My test comment");
		int commentId = (int) CommentStore.storeComment(m1, c1);
		assertEquals(c1, CommentStore.retrieveComment(m1, commentId));
	}
	
	@Test
	public void commentsShouldBeRetrieved() {
		Message m2 = MessageStore.retrieveMessage(2);
		assertEquals("[[1, Pierre, Comment 1], [2, Paul, Comment 2], [3, Jacques, Comment 3]]", 
				CommentStore.retrieveAllComments(m2).toString());
	}

	@Test
	public void commentShouldBeDeleted() {
		Message m3 = new Message("Nyna", "Delete this message");
		MessageStore.storeMessage(m3);
		Comment c1 = new Comment("Nyna", "My comment to delete");
		int commentId = (int) CommentStore.storeComment(m3, c1);
		CommentStore.deleteComment(m3, c1);
		assertNull(CommentStore.retrieveComment(m3, commentId));
	}
}
