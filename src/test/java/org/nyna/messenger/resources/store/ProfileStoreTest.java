package org.nyna.messenger.resources.store;

import org.junit.Test;
import org.nyna.messenger.resources.model.Profile;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class ProfileStoreTest {
    @Test
    public void profileShouldBeStored() {
    	Profile myProfile = new Profile("James", "Blunt", "You're beautifull");
    	String profileId = ProfileStore.storeProfile(myProfile);
    	assertEquals(myProfile, ProfileStore.retrieveProfile(profileId));
    }
    
    @Test
    public void profileShouldBeDeleted() {
    	Profile myProfile = new Profile("James", "Blunt", "You're beautifull");
    	String profileId = ProfileStore.storeProfile(myProfile);
    	ProfileStore.deleteProfile(profileId);
    	assertNull(ProfileStore.retrieveProfile(profileId));
    }
}