package org.nyna.messenger.resources.api;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.grizzly.http.server.HttpServer;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.nyna.messenger.Messenger;
import org.nyna.messenger.resources.model.Message;
import org.nyna.messenger.resources.store.MessageStore;

import static org.junit.Assert.assertEquals;

public class MessageAPITest {

    private static HttpServer server;
    private static WebTarget webTarget;

    @BeforeClass
    public static void setUp() throws Exception {
        server = Messenger.startServer();
        Client c = ClientBuilder.newClient();
        webTarget = c.target(Messenger.BASE_URI);
        
        MessageStore.init();
    }

    @AfterClass
    public static void tearDown() throws Exception {
        server.shutdownNow();
    }
    
    @Test
    public void getMessage() {
    	Invocation.Builder invocationBuilder =  webTarget.path("messages/1").request(MediaType.APPLICATION_JSON);
    	Response response = invocationBuilder.get();
    	 
    	Message message = response.readEntity(Message.class);
    	assertEquals(200, response.getStatus());
    	assertEquals(MessageStore.retrieveMessage(1), message);
    }
    
    @Test
    public void postMessage() {
    	Message myMessage = new Message("Testing post message", "Nyna");
    	Invocation.Builder invocationBuilder =  webTarget.path("messages").request(MediaType.APPLICATION_JSON);
    	Response response = invocationBuilder.post(Entity.entity(myMessage, MediaType.APPLICATION_JSON));
    	assertEquals(200, response.getStatus());
    	assertEquals(myMessage.getMessage(), response.readEntity(Message.class).getMessage());
    }
    
    @Test
    public void putMessage() {
    	Message myMessage = new Message("Updating message 1", "Nyna");
    	Invocation.Builder invocationBuilder =  webTarget.path("messages/1").request(MediaType.APPLICATION_JSON);
    	Response response = invocationBuilder.put(Entity.entity(myMessage, MediaType.APPLICATION_JSON));
    	assertEquals(200, response.getStatus());
    	assertEquals(myMessage.getMessage(), response.readEntity(Message.class).getMessage());
    }
    
    @Test
    public void deleteMessage() {
    	Invocation.Builder invocationBuilder =  webTarget.path("messages/1").request(MediaType.APPLICATION_JSON);
    	Response response = invocationBuilder.delete();
    	assertEquals(200, response.getStatus());
    }
}