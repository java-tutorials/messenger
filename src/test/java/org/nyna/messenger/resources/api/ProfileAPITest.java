package org.nyna.messenger.resources.api;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.grizzly.http.server.HttpServer;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.nyna.messenger.Messenger;
import org.nyna.messenger.resources.model.Profile;
import org.nyna.messenger.resources.store.ProfileStore;

import static org.junit.Assert.assertEquals;

public class ProfileAPITest {

    private static HttpServer server;
    private static WebTarget webTarget;

    @BeforeClass
    public static void setUp() throws Exception {
        server = Messenger.startServer();
        Client c = ClientBuilder.newClient();
        webTarget = c.target(Messenger.BASE_URI);
        ProfileStore.init();
    }

    @AfterClass
    public static void tearDown() throws Exception {
        server.shutdownNow();
    }
    
    @Test
    public void getProfile() {
    	Invocation.Builder invocationBuilder =  webTarget.path("profiles/James.Blunt").request(MediaType.APPLICATION_JSON);
    	Response response = invocationBuilder.get();
    	assertEquals(200, response.getStatus());
    }
    
    @Test
    public void postProfile() {
    	Profile myProfile = new Profile("Bill", "Clinton");
    	Invocation.Builder invocationBuilder =  webTarget.path("profiles").request(MediaType.APPLICATION_JSON);
    	Response response = invocationBuilder.post(Entity.entity(myProfile, MediaType.APPLICATION_JSON));
    	assertEquals(200, response.getStatus());
    }
    
    @Test
    public void putProfile() {
    	Profile myProfile = new Profile("Bill", "Clinton");
    	Invocation.Builder invocationBuilder =  webTarget.path("profiles/1").request(MediaType.APPLICATION_JSON);
    	Response response = invocationBuilder.put(Entity.entity(myProfile, MediaType.APPLICATION_JSON));
    	assertEquals(200, response.getStatus());
    }
    
    @Test
    public void deleteProfile() {
    	Invocation.Builder invocationBuilder =  webTarget.path("profiles/James.Blunt").request(MediaType.APPLICATION_JSON);
    	Response response = invocationBuilder.delete();
    	assertEquals(200, response.getStatus());
    }
}